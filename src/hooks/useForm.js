import { useState } from "react";
import ReactForms from "../index";

/**
 * @param {ReactForm.FormOptions} opts
 * @returns {ReactForm.Form}
 */
export default function useForm(opts) {
    let [ form ] = useState(() => new ReactForms(opts));
    return form;
};