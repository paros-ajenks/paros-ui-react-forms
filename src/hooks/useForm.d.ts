import ReactForm, { ReactFormOpts } from "../ReactForm";

export default function useForm(options: ReactFormOpts): ReactForm;
