import { determineValueGetter, ARRAY_INDEX } from "./utils";

export default class LogicBindings {
    getError = field => this.ctx.getError(field);
    getValue = field => this.ctx.getValue(field);

    setError = (field, error, update) => this.ctx.setError(field, error, update);
    setValue = (ctx) => this.ctx.setValue(ctx);

    delError = field => this.ctx.delError(field);
    delValue = field => this.ctx.delValue(field);

    hasValue = field => this.ctx.hasValue(field);
    isDisabled = field => this.ctx.isDisabled(field);

    constructor(ctx) {
        this.ctx = ctx;
    }

    createOnChange = ({ field, valueGetter, interceptOnChange, handler }) => (e, cb) => {
        let oldError = this.getError(field);
        let value = valueGetter(e, field);

        if(oldError) {
            if(value && value[ARRAY_INDEX]) { // supplied by ArrayFieldAdapter
                this.delError([field, value[ARRAY_INDEX]]);
            } else {
                this.delError(field);
            }
        }

        if(interceptOnChange && handler) {
            let previousValue = this.getValue(field);

            // We allow some fields to intercept onChange so they can perform additional actions before setting state.
            // For example, showing a popup before resetting a form when changing a critical field
            handler({ field, previous: previousValue, value }, (val = value, callback) => {
                this.setValue({ field, previousValue, value: val, callback, update: true });
            });
        } else {
            let previousValue = this.getValue(field);

            this.setValue({
                field,
                value,
                previousValue,
                update: true,
                callback: (field, value) => {
                    if(handler) {
                        return handler(value, field, cb);
                    } else if(typeof cb === "function") {
                        return cb(value, field);
                    }
                }
            });
        }
    }

    createOnBlur = (opts = {}) => (e, metadata = {}) => {
        let { field, validateOnBlur, handler = () => null } = opts;
        let localOverride = opts.hasOwnProperty("validateOnBlur") && opts.validateOnBlur !== undefined;

        if(localOverride ? validateOnBlur : this.ctx.validateOnBlur()) {
            this.ctx.validate(field, () => handler(e), metadata[ARRAY_INDEX]);
        } else return handler(e);
    }

    hookField = ({
        // name,
        forbidNull,
        defaultValue = "",
        validateOnBlur,
        value,
        valueGetter = "target.value",
        valueProp = "value",
        errorProp = "error",
        onChangeProp = "onChange",
        onBlurProp = "onBlur",
        onChange,
        onBlur,
        target,
        props = {},
        interceptOnChange,
        hideWhenDisabled,

        createOnChange = this.createOnChange,
        createOnBlur = this.createOnBlur
    }) => {
        if(!this.hasValue(target) || (forbidNull && this.getValue(target) === null))
            this.setValue({ field: target, value: defaultValue });

        if(value !== undefined)
            this.setValue(target, value);

        let disabled = props.disabled;
        let tooltip = props.tooltip;
        let hidden = undefined;

        if(this.isDisabled(target)) {
            disabled = true;
            tooltip = props.disabledTooltip;
        }

        if(hideWhenDisabled && disabled) {
            hidden = true;
        }

        return {
            [valueProp]: this.getValue(target),
            [errorProp]: this.getError(target),
            [onChangeProp]: createOnChange({
                field: target,
                valueGetter: determineValueGetter(valueGetter),
                interceptOnChange,
                handler: onChange
            }),
            [onBlurProp]: createOnBlur({
                field: target,
                validateOnBlur: validateOnBlur,
                handler: onBlur
            }),
            ...props,
            id: target,
            name: target,
            disabled,
            tooltip,
            hidden
        };
    };

    getLooseProps = ({
        forbidNull,
        defaultValue = "",
        value,
        valueProp = "value",
        errorProp = "error",
        onChangeProp = "onChange",
        onBlurProp = "onBlur",
        onChange,
        onBlur,
        target,
        props = {},
        hideWhenDisabled
    }) => {
        let _val = value;

        if(forbidNull && _val === null)
            _val = defaultValue;

        let disabled = props.disabled;
        let tooltip = props.tooltip;
        let hidden = undefined;

        if(hideWhenDisabled && disabled) {
            hidden = true;
        }

        return {
            [valueProp]: value,
            [errorProp]: props.error,
            [onChangeProp]: onChange,
            [onBlurProp]: onBlur,
            ...props,
            id: target,
            name: target,
            disabled,
            tooltip,
            hidden
        };
    }
}
