import { createNestingContext } from "./utils/contexts";
import { mergeProperties } from "./utils";

/**
 *? MetadataContext allows groups of fields to have unique logic or abilities
 *? For example, editing fields of an object works like so:
 *
 * <FormObject field="PetInformation">
 *      <Field.Name component={Input} type="text" placeholder="Name" label="Name" />
 * </FormObject>
 * 
 * The above example text input is editing the Name property of the PetInformation object in the form.
 */
const CTX = createNestingContext({
    objectProp: null
}, undefined, (src, dest) => ({
    objectProp: mergeProperties(src.objectProp, dest.objectProp) // Nesting <FormObject /> will properly get into sub-props
}));

CTX.displayName = "FormMetadataContext";

export default CTX;