import React from "react";
import withRef from "./hoc/withRef";
import omit from "lodash/omit";

import FormObject from "./FormObject";

export default impl => withRef(class HookedForm extends React.PureComponent {
    componentDidMount() {
        let { onSubmit, initialValues, initialErrors, onChange, onError, validation } = this.props;
        
        if(onSubmit) impl.onSubmit(onSubmit);
        if(onChange) impl.onChange(onChange);
        if(onError) impl.on("error", onError);
        if(initialValues) impl.replaceValues(initialValues, true);
        if(initialErrors) impl.replaceErrors(initialErrors, true);
        if(validation) impl.validation = validation;
    }

    componentWillUnmount() {
        let { onSubmit, onChange, onError } = this.props;
        
        if(onSubmit) impl.offSubmit(onSubmit);
        if(onChange) impl.offChange(onChange);
        if(onError) impl.off("error", onError);
    }

    render() {
        let { as: As = "form", children, field, forwardedRef, ...props } = this.props;

        props = omit(props, "onSubmit", "onChange", "initialValues", "initialErrors", "validation");

        let body = children;

        if(field && field.length) {
            body = (
                <FormObject field={field}>{body}</FormObject>
            );
        }

        return (
            <As {...props} ref={forwardedRef} onSubmit={impl.hookSubmit}>
                {body}
            </As>
        );
    }
});
