interface TemplateProps extends React.AllHTMLAttributes<"div"> {
    field: string;
}

interface Template {
    defaultValue: any;
    valueProp: string;
    valueGetter: string | ((e: any) => any);
    onChangeProp: string;
    errorProp: string;
    forbidNull: boolean;
}

interface Output {
    Component: React.ElementType;
}

interface TemplateOpts {
    field: string;
    output: Output;
    template: Template;
    templateProps: TemplateProps;
}

export function addTemplates(additions: {
    [template: string]: (options: TemplateOpts) => TemplateOpts | void;
}): void;

export function getTemplate(name: string): (options: TemplateOpts) => TemplateOpts;