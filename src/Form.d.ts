import * as React from "react";
import { ReactFormValidation } from "./common";

interface FormProps {
    as?: React.ElementType;
    onSubmit?: Function;
    onChange?: Function;

    initialValues?: {};
    initialErrors?: {};

    field?: string;

    validation?: ReactFormValidation;
}

interface Form extends React.PureComponent<FormProps> {}
export = Form;