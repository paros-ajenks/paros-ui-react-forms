import joi from "joi";

export type ReactFormValidator = (values: {}, errors: {}) => void;
export type ReactFormAsyncValidator = (values: {}, errors: {}) => Promise<void>;
export type ReactFormValidation = joi.ObjectSchema | ReactFormValidator | ReactFormAsyncValidator;