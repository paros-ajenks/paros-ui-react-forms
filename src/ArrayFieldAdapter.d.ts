import * as React from "react";
import ReactForm from "./ReactForm";

interface ArrayFieldAdapterProps {
    value: any[];
    error: string;

    children?: ((value: any, props: {}, i: number) => any);
    fieldComponent: React.Component;
    fieldProps: {};
    
    onChange: Function;
    onBlur: Function;

    keyProp: string;
    valueProp: string;
    onChangeProp: string;
    onBlurProp: string;

    filter?: Function;

    template: string;
    form: ReactForm;
}

export default interface ArrayFieldAdapter extends React.PureComponent<ArrayFieldAdapterProps> {}
