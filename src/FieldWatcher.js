import React from "react";
import MetadataContext from "./MetadataContext";

import { mergeProperties } from "./utils";

export default form => class FieldWatcher extends React.PureComponent {
    static contextType = MetadataContext;

    onUpdate = () => this.forceUpdate();

    componentDidMount() {
        let { field, fields = [] } = this.props;
        let { objectProp } = this.context || {};

        let targets = [field, ...fields];
        for(let field of targets) {
            let key = mergeProperties(objectProp, field);

            form.addField(key);
            form.onChange(key, this.onUpdate);
        }
    }

    componentWillUnmount() {
        let { field, fields = [] } = this.props;
        let { objectProp } = this.context || {};

        let targets = [field, ...fields];
        for(let field of targets) {
            let key = mergeProperties(objectProp, field);
            form.offChange(key, this.onUpdate);
            form.removeField(key);
        }
    }

    render() {
        let { children, component: Component, field, fields = [], ...props } = this.props;
        let { objectProp } = this.context || {};

        let data = [field, ...fields].filter(x => x && x.length).reduce((obj, key) => {
            let field = mergeProperties(objectProp, key);

            return {
                ...obj,
                [key]: form.value(field)
            };
        }, {});

        if(Component) {
            return (
                <Component {...data} {...props} form={form} />
            );
        } else if(typeof children === "function") {
            return children(data, props, form, field) || null;
        }

        throw new Error("FieldWatcher requires either child function or component!");
    }
};
