import * as React from "react";

interface FieldWatcherProps {
    field?: string;
    fields?: string[];

    component?: React.Component;
    children?: any;
}

interface FieldWatcher extends React.PureComponent<FieldWatcherProps> {}
export = FieldWatcher;