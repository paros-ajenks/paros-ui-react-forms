import React from "react";
import omit from "lodash/omit";
import isEqual from "lodash/isEqual";
import { ARRAY_INDEX, clone } from "./utils";

class ArrayFieldRenderer extends React.Component {
    shouldComponentUpdate = props => {
        if(!isEqual(props.fieldProps, this.props.fieldProps)) return true;
        if(!isEqual(props.props.value, this.props.props.value)) return true;
        if(!isEqual(props.props.error, this.props.props.error)) return true;
        if(!isEqual(props.props.disabled, this.props.props.disabled)) return true;
        if(!isEqual(props.monitors, this.props.monitors)) return true;
        if(!isEqual(props.hooks, this.props.hooks)) return true;

        return false;
    }

    render = () => {
        let { Component, children, value, props, i, fieldProps, hooks } = this.props;

        return Component ? (
            <Component {...omit(fieldProps, "keyProp")} {...props} {...hooks} />
        ) : (
            children(value, { ...props, ...hooks }, i)
        );
    }
}

export default class ArrayFieldAdapter extends React.PureComponent {
    onChange = index => (e, callback) => {
        let { value = [], onChange } = this.props;

        let val = clone(value);
        val[index] = e;
        val[ARRAY_INDEX] = index;

        return onChange(val, callback);
    }

    onBlur = i => e => {
        let { onBlur } = this.props;
        return onBlur(e, { [ARRAY_INDEX]: i });
    }

    renderValue = (value, props, i) => {
        let { children, keyProp, fieldComponent, fieldProps, hooks } = this.props;

        return (
            <ArrayFieldRenderer key={`AFA-${keyProp}-${value[keyProp]}-${i}`} i={i}
                Component={fieldComponent}
                keyProp={keyProp}
                fieldProps={fieldProps}
                props={props}
                value={value}
                children={children}
                monitors={this.monitors}
                hooks={hooks} />
        );
    }

    get monitors() {
        return Object.keys(this.props.monitors || {}).reduce((x, k) => ({ ...x, [k]: this.props.form.value(k) }), {});
    }

    render() {
        let { children, value = [], keyProp, valueProp, onChangeProp, fieldComponent, filter, template, form, onBlurProp, disabled } = this.props;

        if(typeof children !== "function" && fieldComponent === undefined)
            throw new Error("Must pass either component property or rendering function in children to ArrayFieldAdapter!");
        if(!keyProp)
            throw new Error("Must pass keyProp to ArrayFieldAdapter!");

        let arr = value;

        if(filter) arr = arr.filter(filter);

        return arr.map((x, i) => {
            let error = form && form.error(`${template.target}[${i}]`);
            if(error === null) error = undefined;

            return this.renderValue(x, {
                [valueProp]: x,
                disabled,
                error,
                [onChangeProp]: this.onChange(i),
                [onBlurProp || "onBlur"]: this.onBlur(i)
            }, i);
        });
    }
}
