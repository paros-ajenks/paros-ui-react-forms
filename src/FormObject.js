import React from "react";
import MetadataContext from "./MetadataContext";

export default ({
    field,
    children
}) => (
    <MetadataContext.Provider value={{ objectProp: field }}>
        {children}
    </MetadataContext.Provider>
);
