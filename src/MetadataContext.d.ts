import * as React from "react";

interface MetadataContextData {
    objectProp: string;
}

interface MetadataContext extends React.Context<MetadataContextData> {}
export = MetadataContext;