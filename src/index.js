import ReactForm from "./ReactForm";
import setupForm from "./Form";
import setupFields from "./Fields";
import * as Utils from "./utils";
import ArrayFieldAdapter from "./ArrayFieldAdapter";
import FormObject from "./FormObject";
import useForm from "./hooks/useForm";
import withRef from "./hoc/withRef";
import templates from "./templates";

export default ReactForm;

export {
    ReactForm,
    Utils,
    setupForm,
    setupFields,
    ArrayFieldAdapter,
    FormObject,
    useForm,
    withRef,
    templates
};
